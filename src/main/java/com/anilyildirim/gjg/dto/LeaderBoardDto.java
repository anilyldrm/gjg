package com.anilyildirim.gjg.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LeaderBoardDto {

    private int rank;

    private double points;

    @JsonProperty("display_name")
    private String displayName;

    private String country;
}
