package com.anilyildirim.gjg.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ScoreDto {

    @JsonProperty("score_worth")
    private double scoreWorth;

    @JsonProperty("user_id")
    private String userId;

    private Long timestamp;
}
