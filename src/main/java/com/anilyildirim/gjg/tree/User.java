package com.anilyildirim.gjg.tree;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User implements Comparable<User> {

    private int rank;

    @NonNull
    Double points;

    @JsonProperty("display_name")
    private String displayName;

    @NonNull
    @JsonProperty("user_id")
    private String userId;

    private String country;

    public User(double points) {
        User user = new User();
        user.setPoints(points);
    }

    @Override
    public int compareTo(User o) {
        int compare = Double.compare(this.points, o.points);
        if (compare == 0) {
            return getUserId().compareTo(o.getUserId());
        } else {
            return compare;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof User)) {
            return false;
        }
        User u = (User) o;
        return Double.compare(this.points, u.points) == 0 && this.getUserId().equals(u.getUserId());
    }
}
