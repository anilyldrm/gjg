package com.anilyildirim.gjg.tree;

import lombok.Data;

@Data
public class RedBlackNode<T extends Comparable<T>> {

    public static final int BLACK = 0;
    public static final int RED = 1;
    public T key;

    public RedBlackNode<T> parent;
    public RedBlackNode<T> left;
    public RedBlackNode<T> right;
    public int numLeft = 0;
    public int numRight = 0;
    public int color;

    RedBlackNode() {
        color = BLACK;
        numLeft = 0;
        numRight = 0;
        parent = null;
        left = null;
        right = null;
    }

    public RedBlackNode(T key) {
        this();
        this.key = key;
    }
}