package com.anilyildirim.gjg.service;

import com.anilyildirim.gjg.tree.RedBlackNode;
import com.anilyildirim.gjg.tree.RedBlackTree;
import com.anilyildirim.gjg.tree.User;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Log4j2
@Service
public class LeaderBoardService {

    private RedBlackTree<User> leaderBoard;

    private Map<String, Double> userIdScoreMap;

    @PostConstruct
    public void init() {
        initialize();
    }

    public void initialize() {
        leaderBoard = new RedBlackTree<>();
        userIdScoreMap = new HashMap<>();
    }

    public List<User> getLeaderBoard() {
        List<User> userList = new ArrayList<>();
        RedBlackNode<User> root = leaderBoard.getRoot();
        loadLeaderBoardIntoList(userList, root);
        return userList;
    }

    public int getRanking(String userId, double score) {
        return leaderBoard.findNumGreater(leaderBoard.getRoot(), User.builder()
                .userId(userId)
                .points(score)
                .build()) + 1;
    }

    public void insertToLeaderBoard(User user) {
        leaderBoard.insert(user);
    }

    public RedBlackNode<User> searchLeaderBoard(User user) {
        return leaderBoard.search(user);
    }

    public void removeFromLeaderBoard(RedBlackNode<User> redBlackNode) {
        leaderBoard.remove(redBlackNode);
    }

    public void putToMap(String userId, double score) {
        userIdScoreMap.put(userId, score);
    }

    public Double getFromMap(String userId) {
        return userIdScoreMap.get(userId);
    }

    public void removeFromMap(String userId) {
        userIdScoreMap.remove(userId);
    }

    private void loadLeaderBoardIntoList(List<User> list, RedBlackNode<User> redBlackNode) {
        if (redBlackNode.getKey() == null)
            return;
        loadLeaderBoardIntoList(list, redBlackNode.getRight());
        list.add(redBlackNode.getKey());
        loadLeaderBoardIntoList(list, redBlackNode.getLeft());
    }

}
