package com.anilyildirim.gjg.service;

import com.anilyildirim.gjg.dto.ScoreDto;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class ScoreService {

    private final UserService userService;

    public ScoreService(UserService userService) {
        this.userService = userService;
    }

    public void submitScore(ScoreDto scoreDto) {
        double scoreWorth = scoreDto.getScoreWorth();
        String userId = scoreDto.getUserId();
        Long timestamp = scoreDto.getTimestamp();

        Double oldScore = userService.removeOldUser(userId);
        userService.addNewUser(scoreWorth + oldScore, userId);
    }

}
