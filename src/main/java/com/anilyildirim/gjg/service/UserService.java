package com.anilyildirim.gjg.service;

import com.anilyildirim.gjg.dto.UserDto;
import com.anilyildirim.gjg.tree.RedBlackNode;
import com.anilyildirim.gjg.tree.User;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class UserService {

    private final LeaderBoardService leaderBoardService;

    public UserService(LeaderBoardService leaderBoardService) {
        this.leaderBoardService = leaderBoardService;
    }

    public User saveUser(UserDto userDto) {
        removeOldUser(userDto.getUserId());
        return saveNewUser(userDto);
    }

    public void addNewUser(double scoreWorth, String userId) {
        User newUser = User.builder()
                .userId(userId)
                .points(scoreWorth)
                .build();
        leaderBoardService.insertToLeaderBoard(newUser);
        leaderBoardService.putToMap(userId, scoreWorth);
    }

    public User getUser(String userUuid) {
        Double score = leaderBoardService.getFromMap(userUuid);
        User user = User.builder()
                .userId(userUuid)
                .points(score)
                .build();
        RedBlackNode<User> found = leaderBoardService.searchLeaderBoard(user);
        return found.getKey();
    }

    public Double removeOldUser(String userId) {
        Double score = leaderBoardService.getFromMap(userId);

        if (score != null) {
            removeOldUser(userId, score);
            return score;
        }
        return 0d;
    }

    private User saveNewUser(UserDto userDto) {
        User newUser = User.builder()
                .userId(userDto.getUserId())
                .points(userDto.getPoints())
                .displayName(userDto.getDisplayName())
                .country(userDto.getCountryIsoCode())
                .build();
        leaderBoardService.insertToLeaderBoard(newUser);
        leaderBoardService.putToMap(userDto.getUserId(), userDto.getPoints());

        return getUser(userDto.getUserId());
    }

    private void removeOldUser(String userId, Double oldScore) {
        User user = User.builder()
                .userId(userId)
                .points(oldScore)
                .build();
        RedBlackNode<User> found = leaderBoardService.searchLeaderBoard(user);
        leaderBoardService.removeFromLeaderBoard(found);
        leaderBoardService.removeFromMap(userId);
    }

}
