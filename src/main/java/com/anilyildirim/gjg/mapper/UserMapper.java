package com.anilyildirim.gjg.mapper;

import com.anilyildirim.gjg.dto.UserDto;
import com.anilyildirim.gjg.service.LeaderBoardService;
import com.anilyildirim.gjg.tree.User;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    private final LeaderBoardService leaderBoardService;

    public UserMapper(LeaderBoardService leaderBoardService) {
        this.leaderBoardService = leaderBoardService;
    }

    public UserDto userToUserDtoMapper(User user) {
        return UserDto.builder()
                .userId(user.getUserId())
                .displayName(user.getDisplayName())
                .points(user.getPoints())
                .rank(leaderBoardService.getRanking(user.getUserId(), user.getPoints()))
                .countryIsoCode(user.getCountry())
                .build();
    }
}
