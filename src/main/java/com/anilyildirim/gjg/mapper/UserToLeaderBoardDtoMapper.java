package com.anilyildirim.gjg.mapper;

import com.anilyildirim.gjg.dto.LeaderBoardDto;
import com.anilyildirim.gjg.service.LeaderBoardService;
import com.anilyildirim.gjg.tree.User;
import org.springframework.stereotype.Component;

@Component
public class UserToLeaderBoardDtoMapper {

    private final LeaderBoardService leaderBoardService;

    public UserToLeaderBoardDtoMapper(LeaderBoardService leaderBoardService) {
        this.leaderBoardService = leaderBoardService;
    }

    public LeaderBoardDto userToLeaderBoardDtoMapper(User user) {
        return LeaderBoardDto.builder()
                .rank(leaderBoardService.getRanking(user.getUserId(), user.getPoints()))
                .points(user.getPoints())
                .displayName(user.getDisplayName())
                .country(user.getCountry())
                .build();
    }
}
