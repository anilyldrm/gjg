package com.anilyildirim.gjg.controller;

import com.anilyildirim.gjg.dto.ScoreDto;
import com.anilyildirim.gjg.service.ScoreService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/score")
public class ScoreController {

    private final ScoreService scoreService;

    public ScoreController(ScoreService scoreService) {
        this.scoreService = scoreService;
    }

    @PostMapping
    public void submit(ScoreDto scoreDto) {
        scoreService.submitScore(scoreDto);
    }
}
