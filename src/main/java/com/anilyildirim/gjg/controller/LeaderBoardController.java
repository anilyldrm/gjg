package com.anilyildirim.gjg.controller;

import com.anilyildirim.gjg.dto.LeaderBoardDto;
import com.anilyildirim.gjg.dto.ScoreDto;
import com.anilyildirim.gjg.mapper.UserToLeaderBoardDtoMapper;
import com.anilyildirim.gjg.service.LeaderBoardService;
import com.anilyildirim.gjg.service.ScoreService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/leaderboard")
public class LeaderBoardController {

    private final LeaderBoardService leaderBoardService;

    private final ScoreService scoreService;

    private final UserToLeaderBoardDtoMapper userToLeaderBoardDtoMapper;

    public LeaderBoardController(LeaderBoardService leaderBoardService,
                                 ScoreService scoreService,
                                 UserToLeaderBoardDtoMapper userToLeaderBoardDtoMapper) {
        this.leaderBoardService = leaderBoardService;
        this.scoreService = scoreService;
        this.userToLeaderBoardDtoMapper = userToLeaderBoardDtoMapper;
    }

    @GetMapping
    public List<LeaderBoardDto> getLeaderBoard() {
        return leaderBoardService.getLeaderBoard()
                .stream()
                .map(userToLeaderBoardDtoMapper::userToLeaderBoardDtoMapper)
                .collect(Collectors.toList());
    }

    @GetMapping("/{countryIsoCode}")
    public List<LeaderBoardDto> getLeaderBoardByCountryIsoCode(@PathVariable String countryIsoCode) {
        return leaderBoardService.getLeaderBoard()
                .stream()
                .filter(user -> user.getCountry().equals(countryIsoCode))
                .map(userToLeaderBoardDtoMapper::userToLeaderBoardDtoMapper)
                .collect(Collectors.toList());
    }

    @GetMapping("/insertDummyScore")
    public void insertDummyScore() {
        for (int i = 0; i < 1000; i++) {
            ScoreDto scoreDto = ScoreDto.builder()
                    .userId(UUID.randomUUID().toString())
                    .scoreWorth(new Random().nextDouble())
                    .build();

            scoreService.submitScore(scoreDto);
        }
    }

    @GetMapping("/reset")
    public void reset() {
        leaderBoardService.initialize();
    }
}
