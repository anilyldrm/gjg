package com.anilyildirim.gjg.controller;

import com.anilyildirim.gjg.dto.UserDto;
import com.anilyildirim.gjg.mapper.UserMapper;
import com.anilyildirim.gjg.service.UserService;
import com.anilyildirim.gjg.tree.User;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    private final UserMapper userMapper;

    public UserController(UserService userService,
                          UserMapper userMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
    }

    @GetMapping("/profile/{userUuid}")
    public UserDto getUser(@PathVariable String userUuid) {
        User user = userService.getUser(userUuid);
        return userMapper.userToUserDtoMapper(user);
    }

    @PostMapping("/user/create")
    public UserDto createUser(@RequestBody UserDto userDto) {
        User user = userService.saveUser(userDto);
        return userMapper.userToUserDtoMapper(user);
    }
}
