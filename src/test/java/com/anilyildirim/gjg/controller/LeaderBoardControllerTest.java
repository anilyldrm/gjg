package com.anilyildirim.gjg.controller;

import com.anilyildirim.gjg.dto.LeaderBoardDto;
import com.anilyildirim.gjg.mapper.UserToLeaderBoardDtoMapper;
import com.anilyildirim.gjg.service.LeaderBoardService;
import com.anilyildirim.gjg.tree.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LeaderBoardControllerTest {

    @InjectMocks
    private LeaderBoardController leaderBoardController;

    @Mock
    private LeaderBoardService leaderBoardService;

    @Mock
    private UserToLeaderBoardDtoMapper userToLeaderBoardDtoMapper;

    @Test
    public void shouldGetLeaderBoard() {
        User user1 = User.builder()
                .userId("b893b1e6-5101-4244-ba32-9fb46e820781")
                .displayName("player1")
                .points(1500d)
                .country("tr")
                .rank(1)
                .build();

        User user2 = User.builder()
                .userId("d6c83c45-59de-4d46-b891-0ddabc3d07cd")
                .displayName("player2")
                .points(800d)
                .country("tr")
                .rank(2)
                .build();

        LeaderBoardDto leaderBoardDto1 = LeaderBoardDto.builder()
                .rank(user1.getRank())
                .points(user1.getPoints())
                .displayName(user1.getDisplayName())
                .country(user1.getCountry())
                .build();

        LeaderBoardDto leaderBoardDto2 = LeaderBoardDto.builder()
                .rank(user2.getRank())
                .points(user2.getPoints())
                .displayName(user2.getDisplayName())
                .country(user2.getCountry())
                .build();

        when(leaderBoardService.getLeaderBoard()).thenReturn(Arrays.asList(user1, user2));
        when(userToLeaderBoardDtoMapper.userToLeaderBoardDtoMapper(user1)).thenReturn(leaderBoardDto1);
        when(userToLeaderBoardDtoMapper.userToLeaderBoardDtoMapper(user2)).thenReturn(leaderBoardDto2);

        List<LeaderBoardDto> leaderBoard = leaderBoardController.getLeaderBoard();

        LeaderBoardDto player1 = leaderBoard.get(0);
        LeaderBoardDto player2 = leaderBoard.get(1);

        assertUserAndLeaderBoardDto(user1, player1);
        assertUserAndLeaderBoardDto(user2, player2);
    }

    private void assertUserAndLeaderBoardDto(User user, LeaderBoardDto player) {
        assertEquals(user.getPoints(), player.getPoints(), 0.001);
        assertEquals(user.getDisplayName(), player.getDisplayName());
        assertEquals(user.getCountry(), player.getCountry());
    }
}