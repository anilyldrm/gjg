package com.anilyildirim.gjg.controller;

import com.anilyildirim.gjg.dto.UserDto;
import com.anilyildirim.gjg.mapper.UserMapper;
import com.anilyildirim.gjg.service.UserService;
import com.anilyildirim.gjg.tree.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserControllerTest {

    @InjectMocks
    private UserController userController;

    @Mock
    private UserService userService;

    @Mock
    private UserMapper userMapper;

    @Test
    public void shouldGetUser() {
        String userId = "8214ba48-8c23-4d3c-a827-66071c914764";
        User user = User.builder()
                .userId(userId)
                .displayName("player1")
                .points(1500d)
                .country("tr")
                .rank(1)
                .build();

        UserDto userDto = UserDto.builder()
                .userId(userId)
                .displayName("player1")
                .points(1500d)
                .rank(1)
                .build();

        when(userService.getUser(userId)).thenReturn(user);
        when(userMapper.userToUserDtoMapper(user)).thenReturn(userDto);
        UserDto returnedUserDto = userController.getUser(userId);

        verify(userService).getUser(userId);
        verify(userMapper).userToUserDtoMapper(user);
        assertEquals(userDto, returnedUserDto);
    }

    @Test
    public void shouldCreateUser() {
        String userId = "8214ba48-8c23-4d3c-a827-66071c914764";
        User user = User.builder()
                .userId(userId)
                .displayName("player1")
                .points(1500d)
                .country("tr")
                .rank(1)
                .build();

        UserDto userDto = UserDto.builder()
                .userId(userId)
                .displayName("player1")
                .points(1500d)
                .rank(1)
                .build();

        when(userService.saveUser(userDto)).thenReturn(user);
        when(userMapper.userToUserDtoMapper(user)).thenReturn(userDto);

        UserDto returnedUserDto = userController.createUser(userDto);
        verify(userService).saveUser(userDto);
        verify(userMapper).userToUserDtoMapper(user);
        assertEquals(userDto, returnedUserDto);
    }
}