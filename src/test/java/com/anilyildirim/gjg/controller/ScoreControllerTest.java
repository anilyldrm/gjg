package com.anilyildirim.gjg.controller;

import com.anilyildirim.gjg.dto.ScoreDto;
import com.anilyildirim.gjg.service.ScoreService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ScoreControllerTest {

    @InjectMocks
    private ScoreController scoreController;

    @Mock
    private ScoreService scoreService;

    @Test
    public void shouldSubmitScore() {
        ScoreDto scoreDto = ScoreDto.builder()
                .userId("500e248c-9cd1-4b21-a701-8d0c61887e35")
                .scoreWorth(1200d)
                .build();
        scoreController.submit(scoreDto);

        verify(scoreService).submitScore(scoreDto);
    }
}