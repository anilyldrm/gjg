package com.anilyildirim.gjg.service;

import com.anilyildirim.gjg.dto.UserDto;
import com.anilyildirim.gjg.tree.RedBlackNode;
import com.anilyildirim.gjg.tree.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @InjectMocks
    private UserService userService;

    @Mock
    private LeaderBoardService leaderBoardService;

    private final String userId = "fc1ff764-fa05-4902-aadc-9911804bfe95";

    @Test
    public void shouldSaveUser() {
        UserDto userDto = UserDto.builder()
                .userId(userId)
                .displayName("player1")
                .points(1500d)
                .rank(1)
                .build();

        User user = User.builder()
                .userId(userDto.getUserId())
                .points(userDto.getPoints())
                .displayName(userDto.getDisplayName())
                .build();

        when(leaderBoardService.getFromMap(userId)).thenReturn(1500d);
        when(leaderBoardService.searchLeaderBoard(user)).thenReturn(new RedBlackNode<>(user));
        User savedUser = userService.saveUser(userDto);

        assertEquals(userId, savedUser.getUserId());
        assertEquals("player1", savedUser.getDisplayName());
        assertEquals(1500d, savedUser.getPoints());
    }

    @Test
    public void shouldAddNewUser() {
        userService.addNewUser(1500d, userId);

        ArgumentCaptor<User> userArgumentCaptor = ArgumentCaptor.forClass(User.class);
        ArgumentCaptor<String> userIdArgumentCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Double> scoreArgumentCaptor = ArgumentCaptor.forClass(Double.class);

        verify(leaderBoardService).insertToLeaderBoard(userArgumentCaptor.capture());
        verify(leaderBoardService).putToMap(userIdArgumentCaptor.capture(), scoreArgumentCaptor.capture());

        assertEquals(1500d, userArgumentCaptor.getValue().getPoints());
        assertEquals(userId, userIdArgumentCaptor.getValue());
        assertEquals(1500d, scoreArgumentCaptor.getValue());
    }

    @Test
    public void shouldGetUser() {
        User user = User.builder()
                .userId(userId)
                .points(1500d)
                .displayName("player1")
                .build();

        when(leaderBoardService.searchLeaderBoard(user)).thenReturn(new RedBlackNode<>(user));
        when(leaderBoardService.getFromMap(userId)).thenReturn(1500d);

        User foundUser = userService.getUser(userId);

        assertEquals(userId, foundUser.getUserId());
    }

    @Test
    public void shouldRemoveOldUserWhenOldUserExists() {
        User user = User.builder()
                .userId(userId)
                .points(1500d)
                .displayName("player1")
                .build();

        when(leaderBoardService.getFromMap(userId)).thenReturn(1500d);
        when(leaderBoardService.searchLeaderBoard(user)).thenReturn(new RedBlackNode<>(user));

        userService.removeOldUser(userId);

        verify(leaderBoardService).removeFromLeaderBoard(any());
        verify(leaderBoardService).removeFromMap(userId);
    }

    @Test
    public void shouldNotRemoveOldUserWhenOldUserNotExists() {
        when(leaderBoardService.getFromMap(userId)).thenReturn(null);
        userService.removeOldUser(userId);

        verify(leaderBoardService, never()).searchLeaderBoard(any(User.class));
        verify(leaderBoardService, never()).removeFromLeaderBoard(any());
        verify(leaderBoardService, never()).removeFromMap(anyString());
    }
}