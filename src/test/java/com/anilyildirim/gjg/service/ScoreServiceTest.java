package com.anilyildirim.gjg.service;

import com.anilyildirim.gjg.dto.ScoreDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ScoreServiceTest {

    @InjectMocks
    private ScoreService scoreService;

    @Mock
    private UserService userService;

    @Test
    public void shouldSubmitScore() {
        String userId = "500e248c-9cd1-4b21-a701-8d0c61887e35";
        double scoreWorth = 1200d;
        ScoreDto scoreDto = ScoreDto.builder()
                .userId(userId)
                .scoreWorth(scoreWorth)
                .build();

        scoreService.submitScore(scoreDto);

        verify(userService).removeOldUser(userId);
        verify(userService).addNewUser(scoreWorth, userId);
    }
}