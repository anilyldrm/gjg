package com.anilyildirim.gjg.service;

import com.anilyildirim.gjg.dto.UserDto;
import com.anilyildirim.gjg.tree.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class LeaderBoardServiceTest {

    @InjectMocks
    private LeaderBoardService leaderBoardService;

    private final String userId = "fc1ff764-fa05-4902-aadc-9911804bfe95";
    private final String userId2 = "42fc15fb-4498-453e-b321-7b477df9dca2";

    @Before
    public void init() {
        leaderBoardService.initialize();
        User user1 = User.builder()
                .userId(userId)
                .displayName("player1")
                .points(1500d)
                .country("tr")
                .rank(1)
                .build();

        UserDto userDto1 = UserDto.builder()
                .userId(userId)
                .displayName("player1")
                .points(1500d)
                .rank(1)
                .build();
        leaderBoardService.insertToLeaderBoard(user1);
        leaderBoardService.putToMap(userDto1.getUserId(), userDto1.getPoints());

        User user2 = User.builder()
                .userId(userId2)
                .displayName("player2")
                .points(800d)
                .country("tr")
                .rank(2)
                .build();

        UserDto userDto2 = UserDto.builder()
                .userId(userId2)
                .displayName("player2")
                .points(800d)
                .rank(2)
                .build();
        leaderBoardService.insertToLeaderBoard(user2);
        leaderBoardService.putToMap(userDto2.getUserId(), userDto2.getPoints());
    }

    @Test
    public void shouldGetLeaderBoard() {
        List<User> leaderBoard = leaderBoardService.getLeaderBoard();
        assertEquals(2, leaderBoard.size());

        User user1 = leaderBoard.get(0);
        assertEquals(userId, user1.getUserId());
        assertEquals("player1", user1.getDisplayName());
        assertEquals(1500d, user1.getPoints());
        assertEquals("tr", user1.getCountry());
        assertEquals(1, user1.getRank());

        User user2 = leaderBoard.get(1);
        assertEquals(userId2, user2.getUserId());
        assertEquals("player2", user2.getDisplayName());
        assertEquals(800d, user2.getPoints());
        assertEquals("tr", user2.getCountry());
        assertEquals(2, user2.getRank());
    }

    @Test
    public void shouldGetRanking() {
        int ranking1 = leaderBoardService.getRanking(userId, 1500d);
        assertEquals(1, ranking1);

        int ranking2 = leaderBoardService.getRanking(userId2, 800d);
        assertEquals(2, ranking2);
    }
}